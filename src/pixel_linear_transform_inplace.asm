; Converts int8 (1st parameter) to a single-precision float32 and fills the xmm register (2nd parameter) with 4 copies of it
%macro packed_floats_from_int8 2
    xor rax, rax
    mov al, %1
    shl rax, 32
    mov al, %1
    movq mm0, rax
    cvtpi2ps %2, mm0
    movlhps %2, %2
%endmacro

section .text
global pixel_linear_transform_inplace

; Passing 3 vectors instead of a matrix to delegate the data alignment to a caller
; void pixel_linear_transform_inplace(struct pixel* pixel, const float blue_column[3], const float green_column[3], const float red_column[3])
pixel_linear_transform_inplace:
    packed_floats_from_int8 [rdi], xmm0
    mulps xmm0, [rsi]

    packed_floats_from_int8 [rdi + 1], xmm1
    mulps xmm1, [rdx]

    packed_floats_from_int8 [rdi + 2], xmm2
    mulps xmm2, [rcx]

    addps xmm0, xmm1
    addps xmm0, xmm2
    cvtps2dq xmm0, xmm0
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    sub rsp, 16
    movd [rsp], xmm0
    mov ax, word [rsp]
    mov word [rdi], ax
    mov al, byte [rsp + 2]
    mov byte [rdi + 2], al
    add rsp, 16
    ret
