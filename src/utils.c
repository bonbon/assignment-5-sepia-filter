#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int print_error(const char* string) {
  return fprintf(stderr, "%s", string);
}

char* string_concat(const char* a, const char* b) {
  char* result = malloc(strlen(a) + strlen(b));
  strcpy(result, a);
  strcat(result, b);
  return result;
}
