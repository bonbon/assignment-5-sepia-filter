#include <inttypes.h>
#include <stdlib.h>
#include "utils.h"
#include "image.h"

extern void pixel_linear_transform_inplace(struct pixel* pixel, const float blue_column[3], const float green_column[3], const float red_column[3]);

static const float sepia_blue_vec[3]  __attribute__((aligned(16))) = { .131f, .168f, .189f };
static const float sepia_green_vec[3] __attribute__((aligned(16))) = { .543f, .686f, .769f };
static const float sepia_red_vec[3]   __attribute__((aligned(16))) = { .272f, .349f, .393f };


static inline uint8_t clamp_pixel_component(uint64_t comp) {
  if (comp > 255) return 255;
  return comp;
}

static void sepia_pixel_inplace(struct pixel* source) {
  struct pixel original = *source;
  source->b = clamp_pixel_component(original.b * sepia_blue_vec[0] + original.g * sepia_green_vec[0] + original.r * sepia_red_vec[0]);
  source->g = clamp_pixel_component(original.b * sepia_blue_vec[1] + original.g * sepia_green_vec[1] + original.r * sepia_red_vec[1]);
  source->r = clamp_pixel_component(original.b * sepia_blue_vec[2] + original.g * sepia_green_vec[2] + original.r * sepia_red_vec[2]);
}

void sepia_c_inplace(struct image* source) {
  if (source->data == NULL) return;
  for (size_t i = 0; i < source->height; i ++) {
    for (size_t j = 0; j < source->width; j ++) {
      uint64_t pixel_index = source->width * i + j;
      sepia_pixel_inplace(source->data + pixel_index);
    }
  }
}

void sepia_asm_inplace(struct image* source) {
  if (source->data == NULL) return;
  for (size_t i = 0; i < source->height; i ++) {
    for (size_t j = 0; j < source->width; j ++) {
      uint64_t pixel_index = source->width * i + j;
      pixel_linear_transform_inplace(source->data + pixel_index, sepia_blue_vec, sepia_green_vec, sepia_red_vec);
    }
  }
}
