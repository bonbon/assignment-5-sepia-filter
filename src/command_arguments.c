#include "command_arguments.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

bool command_arguments_try_get(int argc, char** argv, struct command_arguments* arguments) {
  size_t i = 1;
  arguments->profile = false;
  arguments->source = NULL;
  arguments->target = NULL;
  while (i < argc) {
    char* argument = argv[i];
    if (strcmp("--profile", argument) == 0 || strcmp("-p", argument) == 0) {
      arguments->profile = true;
    } else if (arguments->source == NULL) {
      arguments->source = argument;
    } else if (arguments->target == NULL) {
      arguments->target = argument;
    } else {
      return false;
    }
    i ++;
  }
  return arguments->source != NULL && arguments->target != NULL;
}
