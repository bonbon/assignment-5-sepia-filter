#include "image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static const char *bmp_type_chars = "BM";
#define BMP_TYPE (*((uint16_t*) (void*) bmp_type_chars))

struct  __attribute__((packed)) bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

static size_t compute_row_size(uint32_t image_width) {
  return (sizeof(struct pixel) * 8 * image_width + 31) / 32 * 4;
}

enum bmp_read_status {
  BMP_READ_OK = 0,
  BMP_READ_INVALID_FORMAT,
  BMP_READ_FILE_ERROR,
  BMP_READ_MALLOC_ERROR,
  BMP_READ_COUNT
};

enum bmp_read_status from_bmp(FILE* file, struct image* dist) {
  struct bmp_header header = { 0 };
  const size_t objects_read = fread(&header, sizeof(header), 1, file);
  if (objects_read < 1) {
    return BMP_READ_FILE_ERROR;
  }
  if (header.bfType != BMP_TYPE) {
    return BMP_READ_INVALID_FORMAT;
  }
  
  fseek(file, header.bOffBits, SEEK_SET);
  
  const size_t row_size_bytes = compute_row_size(header.biWidth);
  const size_t row_padding = row_size_bytes - sizeof(struct pixel) * header.biWidth;
  struct pixel* pixels = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
  if (pixels == NULL) {
    return BMP_READ_MALLOC_ERROR;
  }

  for (size_t i = 0; i < header.biHeight; i ++) {
    const size_t current_offset = header.biWidth * i;
    const size_t objects_read = fread(pixels + current_offset, sizeof(struct pixel), header.biWidth, file);
    if (objects_read < header.biWidth) {
      free(pixels);
      return BMP_READ_FILE_ERROR;
    }
    fseek(file, (long) row_padding, SEEK_CUR);
  }

  *dist = (struct image) {
    .width = header.biWidth,
    .height = header.biHeight,
    .data = pixels
  };
  return BMP_READ_OK;
}

enum bmp_write_status {
  BMP_WRITE_OK = 0,
  BMP_WRITE_ERROR,
  BMP_WRITE_COUNT
};

static struct bmp_header bmp_header_from_image(struct image const* image) {
  const size_t row_size_bytes = compute_row_size(image->width);
  const uint32_t image_size = image->height * row_size_bytes;
  
  struct bmp_header header = {
    .bfType = BMP_TYPE,
    .bfileSize = sizeof(struct bmp_header) + image_size,
    .bfReserved = 0,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = sizeof(struct bmp_header) - ((char*) &header.biSize - (char*) &header),
    .biWidth = image->width,
    .biHeight = image->height,
    .biPlanes = 1,
    .biBitCount = sizeof(struct pixel) * 8,
    .biCompression = 0,
    .biSizeImage = image_size,
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0
  };

  return header;
}

enum bmp_write_status to_bmp(FILE* out, struct image const* image) {
  const size_t row_size_bytes = compute_row_size(image->width);
  const struct bmp_header header = bmp_header_from_image(image);

  const size_t objects_written = fwrite(&header, sizeof(header), 1, out);
  if (objects_written < 1) {
    return BMP_WRITE_ERROR;
  }

  const size_t padding = row_size_bytes - sizeof(struct pixel) * image->width;
  const uint32_t zero = 0;
  for (size_t i = 0; i < image->height; i ++) {
    const struct pixel* current_row = image->data + i * image->width;
    size_t objects_written = fwrite(current_row, sizeof(struct pixel), image->width, out);
    if (objects_written < image->width) {
      return BMP_WRITE_ERROR;
    }
    if (padding != 0) {
      size_t objects_written = fwrite(&zero, padding, 1, out);
      if (objects_written < 1) {
        return BMP_WRITE_ERROR;
      }
    }
  }
  return BMP_WRITE_OK;
}
