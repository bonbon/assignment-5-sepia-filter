#include "bmp.h"
#include "command_arguments.h"
#include "image.h"
#include "messages.h"
#include "sepia.h"
#include "utils.h"

#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define PROFILING_SAMPLE_SIZE 100

int main(int argc, char** argv) {
  struct command_arguments args = { 0 };
  const bool args_got = command_arguments_try_get(argc, argv, &args);
  if (!args_got) {
    print_error(USAGE_MESSAGE);
    return 1;
  }
  
  struct image image = { 0 };
  // Reading the .bmp file into `image`
  {
    FILE* source_file = fopen(args.source, "r");
    if (source_file == NULL) {
      perror("Couldn't open the source file");
      return 1;
    }

    const enum bmp_read_status read_status = from_bmp(source_file, &image);
    fclose(source_file);
    if (read_status != BMP_READ_OK) {
      // NOTE: If read_status is not BMP_READ_OK `from_bmp` doesn't allocate any memory
      print_error("Couldn't read BMP image from source file: ");
      print_error(BMP_READ_STATUS_MESSAGES[read_status]);
      print_error("\n");
      return 1;
    }
  }

  // Applying sepia filter to image (with profiling)
  if (args.profile) {
    struct image copy = image_copy(image);
    if (copy.data == NULL) {
      perror("Couldn't allocate necessary memory");
      return 1;
    }

    double spent_seconds_c;
    PROFILE_BLOCK(spent_seconds_c, {
      for (size_t i = 0; i < PROFILING_SAMPLE_SIZE; i ++) {
        sepia_c_inplace(&copy);
      }
    });
    image_free(copy);

    double spent_seconds_asm;
    PROFILE_BLOCK(spent_seconds_asm, {
      for (size_t i = 0; i < PROFILING_SAMPLE_SIZE; i ++) {
        sepia_asm_inplace(&image);
      }
    });

    printf(
      "          | total  | percall\n"
      "sepia_asm | %.3fs | %.3fs\n"
      "sepia_c   | %.3fs | %.3fs\n"
      "(%d calls)\n",
      spent_seconds_asm,
      spent_seconds_asm / PROFILING_SAMPLE_SIZE,
      spent_seconds_c,
      spent_seconds_c / PROFILING_SAMPLE_SIZE,
      PROFILING_SAMPLE_SIZE
    );
  } else {
    {
      struct image image_c = image_copy(image);
      if (image_c.data == NULL) {
        perror("Couldn't allocate necessary memory");
        return 1;
      }
      sepia_c_inplace(&image_c);
      char* target_c = string_concat("c-", args.target);
      FILE* output_file = fopen(target_c, "w");
      if (output_file == NULL) {
        perror("Couldn't open the output file");
      }
      const enum bmp_write_status write_status = to_bmp(output_file, &image_c);
      fclose(output_file);
      image_free(image_c);
      free(target_c);
      if (write_status != BMP_WRITE_OK) {
        print_error("Couldn't write BMP image to output file: ");
        print_error(BMP_WRITE_STATUS_MESSAGES[write_status]);
        print_error("\n");
        return 1;
      }
    }

    {
      sepia_asm_inplace(&image);
      char* target_asm = string_concat("asm-", args.target);
      FILE* output_file = fopen(target_asm, "w");
      if (output_file == NULL) {
        perror("Couldn't open the output file");
      }
      const enum bmp_write_status write_status = to_bmp(output_file, &image);
      fclose(output_file);
      image_free(image);
      free(target_asm);
      if (write_status != BMP_WRITE_OK) {
        print_error("Couldn't write BMP image to output file: ");
        print_error(BMP_WRITE_STATUS_MESSAGES[write_status]);
        print_error("\n");
        return 1;
      }
    }
  }
	return 0;
}
