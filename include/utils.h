#pragma once
#include <time.h>

#define PROFILE_BLOCK(timing_var, code_block) {           \
    clock_t begin = clock();                              \
    code_block;                                           \
    clock_t end = clock();                                \
    timing_var = (double) (end - begin) / CLOCKS_PER_SEC; \
}

int print_error(const char* string);

char* string_concat(const char* a, const char* b);
