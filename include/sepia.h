#pragma once

#include "image.h"

void sepia_c_inplace(struct image* source);
void sepia_asm_inplace(struct image* source);