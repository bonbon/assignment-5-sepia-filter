#pragma once

#include "bmp.h"

const char* const USAGE_MESSAGE = "usage:\n\
  sepia.out [...OPTIONS] SOURCE_IMAGE OUTPUT_IMAGE\n\
  --profile, -p  Run profiling, comparing asm and c sepia implementations\n";

const char* const BMP_WRITE_STATUS_MESSAGES[BMP_WRITE_COUNT] = {
  [BMP_WRITE_OK] = "Ok",
  [BMP_WRITE_ERROR] = "Couldn't write to the file"
};

const char* const BMP_READ_STATUS_MESSAGES[BMP_READ_COUNT] = {
  [BMP_READ_OK] = "Ok",
  [BMP_READ_INVALID_FORMAT] = "Invalid format",
  [BMP_READ_FILE_ERROR] = "Couldn't read from file",
  [BMP_READ_MALLOC_ERROR] = "Couldn't allocate neccessary memory"
};
