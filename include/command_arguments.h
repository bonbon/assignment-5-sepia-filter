#pragma once

#include <inttypes.h>
#include <stdbool.h>

struct command_arguments {
  const char* source;
  const char* target;
  bool profile;
};

bool command_arguments_try_get(int argc, char** argv, struct command_arguments* arguments);
